﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomAuthenticationMVC.Models
{
    public class QuestionView
    {
        public int QuestionId { get; set; }
        public string QuestionTags { get; set; }
        public string QuestionText { get; set; }
        public string QuestionDescription { get; set; }
        public string QuestionPicPath { get; set; }
        public string RightAnswer { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string OptionE { get; set; }
        public string OptionF { get; set; }
        public bool IsReady { get; set; }
    }

    public class TagView
    {
        public string name { get; set; }
        public string id { get; set; }
    }
}