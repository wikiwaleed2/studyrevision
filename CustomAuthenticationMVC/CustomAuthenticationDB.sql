create table dbo.users (
userId int identity(1,1) not null,
username varchar(50),
firstName varchar(50),
lastName varchar(50),
email varchar(50),
password varchar(50),
isActive bit,
activationCode uniqueidentifier 
);
-------------------------------------------------
create table dbo.roles(
roleId int identity(1,1) not null,
roleName varchar(50) not null
);
-------------------------------------------------
create table dbo.userRoles(
userId int not null,
roleId int not null
);
-------------------------------------------------
create table dbo.questions(
questionId int identity(1,1) not null,
questionTags varchar(50) not null,
questionText varchar(max) not null,
questionDescription varchar(max),
questionPicPath varchar(max),
rightAnswer varchar(max),
optionA varchar(max),
optionB varchar(max),
optionC varchar(max),
optionD varchar(max),
optionE varchar(max),
optionF varchar(max),
addedBy varchar(60),
isReady bit,
createdOn date,
editedOn date
)
---------------------------------------
create table dbo.tags(
tagId int identity(1,1) not null,
tagName varchar(50) not null,
parentTag varchar(50)
)
------------------------------------------------
create table dbo.answers (
answerId int identity(1,1) not null,
questionId int not null,
username varchar(60) not null,
givenAnswer varchar(max),
answerDate date,
quizId int,
);
-----------------------------------------------











Alter PROCEDURE GetQuestions @username varchar(60), @addedBy varchar(60), @isReady int
AS
DECLARE @questionLimit int;
DECLARE @attempted int;
DECLARE @total int;
DECLARE @notAttempted int;
Set @questionLimit = 100;


---------------------------------------------------------------------------------------
select @notAttempted = Count(*) from questions where questionId not in 
(
select distinct questionId from answers where username = @username
) and isnull(addedBy,'') like '%'+@addedBy+'%'
and isReady >= @isReady
---------------------------------------------------------------------------------------


--case when addedBy != ''
--then
--@addedBy
--end


if(@notAttempted > @questionLimit)
begin
---------------------------------------------------------------------------------------
select top(@questionLimit)* from questions where questionId not in 
(
select distinct questionId from answers where username = @username
) and isnull(addedBy,'') like '%'+@addedBy+'%'
and isReady >= @isReady
---------------------------------------------------------------------------------------
end

else
begin


CREATE TABLE #TEMPTABLE
(
	id int Identity not null,
    questionId int
)
--------------------------------------------------
INSERT INTO #TEMPTABLE
---------------------------------------------------------------------------------------
select questionId from questions where questionId not in 
(
select distinct questionId from answers where username = @username
) and isnull(addedBy,'') like '%'+@addedBy+'%'
and isReady >= @isReady
---------------------------------------------------------------------------------------

INSERT INTO #TEMPTABLE
---------------------------------------------------------------------------------------
select  questionId from (
select top(@questionLimit)
	answers.questionId,
	COUNT(answers.questionId) as times,
	MAX(answerDate) as maxdate ,
	DATEDIFF(hour, max(answerDate), GETDATE()) AS DateDiff
from answers inner join questions on answers.questionId = questions.questionId where username = @username  
and isnull(addedBy,'') like '%'+@addedBy+'%' 
and isReady >= @isReady 
group by answers.questionId

) as t order by (t.DATEDIFF/t.times) desc
---------------------------------------------------------------------------------------

select top(@questionLimit)t.id,q.* from questions q inner join #TEMPTABLE t on q.questionId = t.questionId
order by t.id
-------------------------------------------------
DROP TABLE #TEMPTABLE
-------------------------------------------------
end

GO


--exec GetQuestions 'waleed','',0