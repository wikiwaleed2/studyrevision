﻿using CustomAuthenticationMVC.DataAccess;
using CustomAuthenticationMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using PagedList.Mvc;
namespace CustomAuthenticationMVC.Controllers
{
    public partial class QuizController : Controller
    {
        // GET: Quiz
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult AddQuestion()
        {
            var question = new Question();
            if(TempData["tags"] != null)
            question.QuestionTags = TempData["tags"].ToString();
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                //string query = "select distinct questionSubject FROM questions";
                //var subjects = dbContext.Database.SqlQuery<string>(query).ToList();
            }
            return View(question);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditQuestion(int questionId)
        {
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                var question = dbContext.Questions.Find(questionId);
                TempData.Keep();
                return View(question);
            }
        }

        [Authorize]
        [HttpGet]
        public JsonResult GetTopics(string subject)
        {
            //var topics = new List<string>();
            //using (AuthenticationDB dbContext = new AuthenticationDB())
            //{
            //    string query = "select distinct questionTopic FROM questions WHERE questionSubject = @p0";
            //    topics = dbContext.Database.SqlQuery<string>(query, subject).ToList();
            //}
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AddTag(string tagName)
        {
            if (ModelState.IsValid)
            {
                using (AuthenticationDB dbContext = new AuthenticationDB())
                {
                    var tag = new Tag();
                    tag.TagName = tagName;
                    dbContext.Tags.Add(tag);
                    dbContext.SaveChanges();
                }
                return Json("Tag Added", JsonRequestBehavior.AllowGet);
            }
            return Json("Tag Adding Failed", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetTags()
        {
            
            var tags = new List<Tag>();
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                tags = dbContext.Tags.ToList();
            }
            return Json(tags, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAllQuestions(string sortOrder, string sortParam, string currentFilter, string searchString, string userString, string userFilter, string completedString, string completedFilter,  int? page)
        {
            TempData["CurrentSort"] = sortOrder;
            TempData["NameSortParm"] = String.IsNullOrEmpty(sortParam) ? "date_desc" : sortParam;
            TempData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            TempData["CurrentFilter"] = searchString;

            if (userString != null)
            {
                page = 1;
            }
            else
            {
                userString = userFilter;
            }
            TempData["UserFilter"] = userString;


            if (completedString != null)
            {
                page = 1;
            }
            else
            {
                completedString = completedFilter;
            }
            TempData["CompletedFilter"] = completedString;

            var questions = new List<Question>().AsQueryable();
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                questions = dbContext.Questions;
                if (!String.IsNullOrEmpty(searchString))
                {
                    questions = questions.Where(s => s.QuestionTags.Contains(searchString)
                                           || s.QuestionText.Contains(searchString));
                }
                if (!String.IsNullOrEmpty(userString))
                {
                    questions = questions.Where(s => s.AddedBy.Equals(userString)
                                           || s.AddedBy.Contains(userString));
                }
                if (!String.IsNullOrEmpty(completedString))
                {
                    questions = questions.Where(s => s.IsReady.Equals(true));
                }
                switch (sortParam)
                {
                    case "tags":
                        questions = questions.OrderBy(q => q.QuestionTags);
                        break;
                    case "question":
                        questions = questions.OrderBy(q => q.QuestionText);
                        break;
                    //case "Date":
                    //    questions = questions.OrderBy(s => s.EnrollmentDate);
                    //    break;
                    case "date_desc":
                        questions = questions.OrderByDescending(q => q.CreatedOn);
                        break;
                    default:
                        questions = questions.OrderByDescending(q => q.CreatedOn);
                        break;
                }
                int pageSize = 10;
                int pageNumber = (page ?? 1);
                TempData["pageNumber"] = pageNumber;
                return View(questions.ToPagedList(pageNumber, pageSize));
            }
        }

        [HttpPost]
        public ActionResult EditQuestion(QuestionView questionView, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                using (AuthenticationDB dbContext = new AuthenticationDB())
                {
                    var question = dbContext.Questions.First(q => q.QuestionId == questionView.QuestionId);
                    question.QuestionTags = questionView.QuestionTags;
                    question.QuestionText = questionView.QuestionText;
                    question.QuestionDescription = questionView.QuestionDescription;
                    question.QuestionPicPath = questionView.QuestionPicPath;
                    question.RightAnswer = questionView.RightAnswer;
                    question.OptionA = questionView.OptionA;
                    question.OptionB = questionView.OptionB;
                    question.OptionC = questionView.OptionC;
                    question.OptionD = questionView.OptionD;
                    question.OptionE = questionView.OptionE;
                    question.OptionF = questionView.OptionF;
                    question.IsReady = (question.OptionA != null && question.OptionA != null && question.RightAnswer != null) ? true : false;
                    question.AddedBy = User.Identity.Name;
                    question.EditedOn = DateTime.Now;
                    dbContext.SaveChanges();
                    if (continueEditing)
                    {
                        question = dbContext.Questions.First(q => q.QuestionTags == questionView.QuestionTags && q.IsReady == false);
                        return RedirectToAction("EditQuestion", new { questionId = question.QuestionId });
                    }
                }
                
                return RedirectToAction("GetAllQuestions", new { page = TempData["pageNumber"], sortParam = TempData["NameSortParm"], sortOrder = TempData["CurrentSort"], currentFilter = TempData["CurrentFilter"], userFilter = TempData["UserFilter"], completedFilter = TempData["CompletedFilter"] });
            }
            ModelState.AddModelError("", "Something Wrong");
            TempData["tags"] = questionView.QuestionTags;
            return View();
        }

        [HttpPost]
        public ActionResult AddQuestion(QuestionView questionView)
        {
            if (ModelState.IsValid)
            {
                using (AuthenticationDB dbContext = new AuthenticationDB())
                {
                    var question = new Question();
                    question.QuestionTags = questionView.QuestionTags;
                    question.QuestionText = questionView.QuestionText;
                    question.QuestionDescription = questionView.QuestionDescription;
                    question.QuestionPicPath = questionView.QuestionPicPath;
                    question.RightAnswer = questionView.RightAnswer;
                    question.OptionA = questionView.OptionA;
                    question.OptionB = questionView.OptionB;
                    question.OptionC = questionView.OptionC;
                    question.OptionD = questionView.OptionD;
                    question.OptionE = questionView.OptionE;
                    question.OptionF = questionView.OptionF;
                    question.IsReady = (question.OptionA != null && question.OptionA != null && question.RightAnswer != null) ? true : false;
                    question.AddedBy = User.Identity.Name;
                    question.CreatedOn = DateTime.Now;
                    dbContext.Questions.Add(question);
                    dbContext.SaveChanges();
                }
                TempData["tags"] = questionView.QuestionTags; ;
                return RedirectToAction("AddQuestion");
            }
            ModelState.AddModelError("", "Something Wrong");
            TempData["tags"] = questionView.QuestionTags;
            return View();
        }

        [HttpGet]
        public ActionResult DeleteQuestion(int questionId)
        {
            if (ModelState.IsValid)
            {
                using (AuthenticationDB dbContext = new AuthenticationDB())
                {
                    var question = dbContext.Questions.First(q => q.QuestionId == questionId);
                    dbContext.Questions.Remove(question);
                    dbContext.SaveChanges();
                }
                return RedirectToAction("GetAllQuestions", new { page = TempData["pageNumber"], sortParam = TempData["NameSortParm"], sortOrder = TempData["CurrentSort"], currentFilter = TempData["CurrentFilter"], userFilter = TempData["UserFilter"], completedFilter = TempData["CompletedFilter"] });
            }
            ModelState.AddModelError("", "Something Wrong");
            return View();
        }
    }
}