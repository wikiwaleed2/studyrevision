﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CustomAuthenticationMVC.DataAccess
{
    public class Question
    {
        public int QuestionId { get; set; }
        public string QuestionTags { get; set; }
        public string QuestionText { get; set; }
        public string QuestionDescription { get; set; }
        public string QuestionPicPath { get; set; }
        public string RightAnswer { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string OptionE { get; set; }
        public string OptionF { get; set; }
        public string AddedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? EditedOn { get; set; }
        public bool IsReady { get; set; }
    }

    public class Answer
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public string Username { get; set; }
        public string AnswerGiven { get; set; }
    }

    public class Tag
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
    }
}