﻿using CustomAuthenticationMVC.DataAccess;
using CustomAuthenticationMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using PagedList;
using PagedList.Mvc;
using System.Data.SqlClient;

namespace CustomAuthenticationMVC.Controllers
{
    public partial class QuizController : Controller
    {
        // GET: Quiz
        [HttpGet]
        public ActionResult DisplayQuiz(string mode)
        {
            ViewBag.Mode = mode;
            
            using (AuthenticationDB dbContext = new AuthenticationDB())
            {
                ViewBag.QuizId = dbContext.Database.SqlQuery<int>("select isnull(max(quizId),1) from answers").FirstOrDefault() + 1;
            }
            return View();
        }

        [HttpGet]
        public JsonResult GetQuizQuestions(string tagName)
        {

            
                if (ModelState.IsValid)
                {
                    using (AuthenticationDB dbContext = new AuthenticationDB())
                    {
                        var usernameParameter = new SqlParameter("@username", User.Identity.Name);
                        var addedByParameter = new SqlParameter("@addedBy", string.Empty);
                        var isReadyParameter = new SqlParameter("@isReady", Convert.ToInt32(0));
                    var result = dbContext.Database
                            .SqlQuery<Question>("GetQuestions @username,@addedBy,@isReady", usernameParameter, addedByParameter, isReadyParameter)
                            .ToList();
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("Tag Adding Failed", JsonRequestBehavior.AllowGet);
            
        }

        [HttpGet]
        public ActionResult InteractiveQuiz()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RealQuiz()
        {
            return View();
        }
    }
}